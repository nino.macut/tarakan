﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    public Animator animator;
    public GameObject PUScope;
    private bool UsingIronSights = false;
    private bool UsingPUScope = false;
    private bool FirstScopeIn = true;
    private bool Scoped = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1)) //If the player is aiming down sights.
        {
            if (FirstScopeIn)
            {
                FirstScopeIn = false;
                if(PUScope.activeInHierarchy == true)
                {
                    Scoped = true;
                }
            }

            if(Scoped == true)
            {
                UsingPUScope = !UsingPUScope;
                animator.SetBool("PU Scope", UsingPUScope);
            }
            else
            {
                UsingIronSights = !UsingIronSights;
                animator.SetBool("Iron Sights", UsingIronSights);
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            animator.SetTrigger("Firing"); //If the player is firing.
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            animator.SetTrigger("Cocking"); //If the player is cocking the bolt.
        }

        if (Input.GetKeyDown(KeyCode.Mouse2))
        {
            if (Scoped)
            {
                Scoped = false;
                UsingPUScope = false;
                animator.SetBool("PU Scope", UsingPUScope);
                UsingIronSights = true;
                animator.SetBool("Iron Sights", UsingIronSights);
                animator.SetTrigger("To Iron Sights");
            }
            else
            {
                Scoped = true;
                UsingPUScope = true;
                animator.SetBool("PU Scope", UsingPUScope);
                UsingIronSights = false;
                animator.SetBool("Iron Sights", UsingIronSights);
                animator.SetTrigger("To Scope");
            }
        }


        //TOGGLE PU SCOPE FOR TESTING WITH KEY P
        /*
        if (Input.GetKeyDown(KeyCode.P))
        {
            PUScope.SetActive(!PUScope.activeInHierarchy);
        }
        */
        
    }
}
