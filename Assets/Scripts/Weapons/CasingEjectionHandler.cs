﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CasingEjectionHandler : MonoBehaviour
{
    public Transform ejectionPort;
    public Rigidbody casingPrefab;
    public float velocity = 20f;

    private void EjectCasing()
    {
        var casing = (Rigidbody)Instantiate(casingPrefab, ejectionPort.position, Quaternion.Euler(new Vector3(0, Random.Range(70, 130), Random.Range(-20,20))));
        casing.velocity = ejectionPort.forward * velocity;
    }
}
