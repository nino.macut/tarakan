﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPhysicsHandler : MonoBehaviour
{
    public Transform barrelTransform;
    public Rigidbody bulletPrefab;
    public float velocity = 800f;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            
            var bullet = (Rigidbody)Instantiate(bulletPrefab, barrelTransform.position, Quaternion.Euler(new Vector3(barrelTransform.rotation.x, barrelTransform.rotation.y, barrelTransform.rotation.z)));
            bullet.velocity = barrelTransform.forward * velocity;
        }
    }
}
