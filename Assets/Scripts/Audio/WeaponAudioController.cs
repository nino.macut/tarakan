﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class WeaponAudioController : MonoBehaviour
{
    public AudioSource[] audioData;
    public AudioClip boltAction;
    public AudioClip fire;

    public void BoltAction()
    {
        audioData[0].clip = boltAction;
        audioData[0].Play(0);
    }

    public void Fire()
    {
        audioData[1].clip = fire;
        audioData[1].Play(0);
    }
}
